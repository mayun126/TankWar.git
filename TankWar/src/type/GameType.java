package type;
/**
 * 游戏模式
 */
public enum GameType {
	/**
	 * 单人模式
	 */
	ONE_PALYER,
	/**
	 * 双人模式
	 */
	TWO_PLAYER,
}
